#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Se importa el random y se pide tamano al usuario
import random
matrix = []
cantidad_alumnos = int(input("Ingrese la cantidad de alumnos"))
cantidad_notas = int(input("Ingrese las notas"))


# Se creara la matriz por medio de la funcion
def creacion_matrix(matrix, cantidad_alumnos, cantidad_notas):
    print("\n")
    print("notas de los estudiantes:")
    # Se recorreran filas y columnas de tamano
    for i in range(cantidad_alumnos):
        print("\n")
        matrix.append([])
        for j in range(cantidad_notas):
            matrix[i].append([])
            # La matriz tendra valores aleatorios
            matrix[i][j] = round(random.uniform(1, 7), 1)
        print("El estudiante:", i, "tiene estas notas:", matrix[i])


# Se crea la funcion aprobar segun el alumno y sus notas
def aprobar(matrix, cantidad_alumnos, cantidad_notas):
    suma_notas = 0
    promedio = 0
    aprobar = 0
    reprobar = 0
    # Se recorrera las fila
    for i in range(cantidad_alumnos):
        # se calculara la suma de cada alumno de sus notas
        suma_notas = sum(matrix[i])
        # se calcula el promedio del alumno
        promedio = suma_notas / cantidad_notas
        # Se analiza si este aprueba
        if (promedio >= 4):
            print("El estudiante:", i, "aprueba", format(promedio))
            aprobar = aprobar + 1
            # De modo contrario el alumno reprueba
        else:
            print("El estudiante:", i, "reprueba", format(promedio))
            reprobar = reprobar + 1
    print("\n")
    # Se imprimen los alumnos que reprueban o aprueban
    print("Los alumnos que reprobaron fueron:", reprobar)
    print("Los alumnos que aprobaron fueron:", aprobar)


# Se llama a las funciones
creacion_matrix(matrix, cantidad_alumnos, cantidad_notas)
print("\n")
aprobar(matrix, cantidad_alumnos, cantidad_notas)
