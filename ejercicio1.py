#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Se define el tamaño de la lista
lista = []
# El valor de la tupla estara definido
tupla = ("hola", -2, "como", "estas", 10)


# Se define la funcion que reemplazara los valores de la tupla
def reemplazo(lista, tupla):
    lista = [tupla[4], tupla[0], tupla[1], tupla[2], tupla[3]]
    print("La tupla reemplazada sera:", lista)


# Se imprime la tupla original y se llama a la funcion
reemplazo(lista, tupla)
print("La tupla original es:", tupla)