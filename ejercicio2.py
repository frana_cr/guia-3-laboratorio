#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Se importa el random y se pide tamano al usuario
import random
matrix = []
numero_columnas = 12
numero_filas = 15


# Se creara la matrix por medio de la funcion
def creacion_matrix(matrix, numero_columnas, numero_filas):
    print("\n")
    print("La matrix es:")
    # Se recorreran filas y columnas de tamano
    for i in range(numero_filas):
        print("\n")
        matrix.append([])
        for j in range(numero_columnas):
            matrix[i].append([])
            # La matrix tendra valores aleatorios
            matrix[i][j] = random.randint(-20, 30)
        # Se imprime la matrix
        print(matrix[i])


# Se busca el numero menor de la matrix
def busqueda_menor(matrix, numero_columnas, numero_filas):
    contador_menor = 11
    for i in range(numero_filas):
        for j in range(numero_columnas):
            if(matrix[i][j] < contador_menor):
                contador_menor = matrix[i][j]
                # Se imprime el numero menor
    print("El numero menor sera:", contador_menor)


# Se busca la suma de los elementos de las cinco primeras columnas
def suma_elementos(matrix, numero_filas):
    suma = 0
    for i in range(numero_filas):
        for j in range(5):
            suma = suma + matrix[i][j]
            # Se imprime la suma de las primeras columnas
    print("La suma de las 5 primeras columnas son:", suma)


# Se buscan los numeros negativos de la columna 5 a la 9
def numeros_negativos(matrix, numero_filas):
    arreglo = []
    arreglo2 = []
    # Se recorre la matrix por la posicion especifica
    for i in range(numero_filas):
        arreglo.append(matrix[i][4:9])
    for j in range(len(arreglo)):
        for k in range(5):
            if(arreglo[j][k] < 0):
                arreglo2.append(arreglo[j][k])
    # Se imprimen los numeros negativos
    print("Los negativos son:", arreglo2, sep=" ", end=" ")


# Se llama a las funciones
creacion_matrix(matrix, numero_columnas, numero_filas)
busqueda_menor(matrix, numero_columnas, numero_filas)
suma_elementos(matrix, numero_filas)
numeros_negativos(matrix, numero_filas)